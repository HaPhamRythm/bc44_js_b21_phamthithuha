
var dssv = [];

var dataJson = localStorage.getItem("DSSV_LOCAL");
if(dataJson !=null){
    var dsArr = JSON.parse(dataJson);
    for(var i=0; i<dsArr.length;i++){
        var sv = new SinhVien (dsArr[i].ma,dsArr[i].ten,dsArr[i].email,dsArr[i].pass,dsArr[i].toan,dsArr[i].ly,dsArr[i].hoa)
        dssv.push(sv);
    }
    renderLayout(dssv);
}


function themSinhVien() {
    // take value of users' input
    var sv = getInfoFromForm ();
    dssv.push(sv);
    
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV_LOCAL",dataJson);
    
    renderLayout(dssv);
}


function deleteSv(id) {
    var indexDelete = -1;
    for (var i=0; i<dssv.length; i++) {
        if (dssv[i].ma == id) {
            indexDelete = i;
        }
    }
dssv.splice(indexDelete,1);
renderLayout(dssv);
}

function editSv(id) {
    var indexEdit = dssv.findIndex(function(item){
        return item.ma == id;
    })
    
        document.getElementById("txtMaSV").disabled = true;
        showInfoToForm(dssv[indexEdit]);
    
    
}

function capNhatSinhVien() {
    var sv = getInfoFromForm ();
    var indexUpdate = dssv.findIndex(function(item){
        return item.ma== sv.ma;
    })
    if(indexUpdate !=-1){
        dssv[indexUpdate] = sv;
        renderLayout(dssv);
    }
    
    
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV_LOCAL",dataJson);
    
    renderLayout(dssv);
    resetForm();
    document.getElementById("txtMaSV").disabled = false;
} 

function resetForm() {
    document.getElementById("formQLSV").reset();
}