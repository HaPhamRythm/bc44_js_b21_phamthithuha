function getInfoFromForm (){
    var ma = document.getElementById("txtMaSV").value;
    var ten = document.getElementById("txtTenSV").value;
    var email = document.getElementById("txtEmail").value;
    var pass = document.getElementById("txtPass").value;
    var toan = document.getElementById("txtDiemToan").value*1;
    var ly = document.getElementById("txtDiemLy").value*1;
    var hoa = document.getElementById("txtDiemHoa").value*1;

    //create object

    var sv = new SinhVien (ma,ten, email, pass, toan,ly, hoa);
    
    return sv;
}

function renderLayout(dssv) {
    content = "";
    for (var i=0; i<dssv.length; i++) {
        
        var trTag = `<tr>
        <td>${dssv[i].ma}</td>
        <td>${dssv[i].ten}</td>
        <td>${dssv[i].email}</td>
        <td>${dssv[i].tinhDTB().toFixed(2)}</td>
        <td>
        <button onclick="editSv(${dssv[i].ma})" class="btn btn-success">Edit</button>
        <button onclick="deleteSv(${dssv[i].ma})" class="btn btn-danger">Delete</button>
        </td>
        </tr>`
        content += trTag;
    }

    document.getElementById("tbodySinhVien").innerHTML = content;
}

function showInfoToForm (sv){
    document.getElementById("txtMaSV").value = sv.ma;
    document.getElementById("txtTenSV").value = sv.ten;
    document.getElementById("txtEmail").value = sv.email;
    document.getElementById("txtPass").value = sv.pass;
    document.getElementById("txtDiemToan").value = sv.toan;
    document.getElementById("txtDiemLy").value = sv.ly;
    document.getElementById("txtDiemHoa").value = sv.hoa;
}